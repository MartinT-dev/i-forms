﻿using I_Form.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace I_Form.Models
{
    public class OptionViewModel
    {
       public Guid Id { get; set; }

        public Guid QuestionId { get; set; }
        public OptionsQuestion Question { get; set; }
        public string Text { get; set; }

    }
}
