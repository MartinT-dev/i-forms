﻿using System;

namespace I_Form.Models
{
    public class TextQuestionViewModel
    {
        public Guid Id{ get; set; }

        public string Title { get; set; }

        public string  Description { get; set; }

        public Guid FormId { get; set; }

        public bool IsRequired { get; set; }
        public bool IsLongAnswer { get; set; }
        public string Content { get; set; }

    }
}