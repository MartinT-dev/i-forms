﻿using I_Form.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace I_Form.Models
{
    public class FormViewModel
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string AuthorName { get; set; }

        public Guid AuthorId { get; set; }

        public ICollection<TextQuestionViewModel> TextQuestions { get; set; }

        public FormViewModel()
        {
            TextQuestions = new List<TextQuestionViewModel>();
        }
    }
}
