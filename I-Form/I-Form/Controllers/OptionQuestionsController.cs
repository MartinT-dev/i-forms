﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using I_Form.Data.Entities;
using I_Form.Models;
using I_Form.Services.Contracts;
using I_Form.Services.DTOEntities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Form.Controllers
{
    public class OptionQuestionsController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IOptionsQuestionService _optionsQuestionService;

        public OptionQuestionsController(IMapper mapper,IOptionsQuestionService optionsQuestionService)
        {
            this._mapper = mapper ?? throw new ArgumentNullException(nameof(mapper)); ;
            this._optionsQuestionService = optionsQuestionService ?? throw new ArgumentNullException(nameof(optionsQuestionService));
        }
        // GET: OptionQuestions
        public async Task<IActionResult> Index(Guid formId)
        {
            var questionsDTO = await _optionsQuestionService.GetAllAsync(formId);
            var questionsVM = _mapper.Map<ICollection<OptionsQuestionViewModel>>(questionsDTO);
            return View(questionsVM);
        }

        // GET: OptionQuestions/Details/5
        public async Task<IActionResult> Details(Guid id)
        {
          if (id == null)
            {
                return NotFound();
            }

            var question = await _optionsQuestionService.GetOptionsQuestion(id);

            var questionVM = _mapper.Map<OptionsQuestionViewModel>(question);

            if (questionVM == null)
            {
                return NotFound();
            }

            return View(questionVM);
        }

        // GET: OptionQuestions/Create
        [HttpGet]
        public ActionResult Create()
        {
            var optionQuestion = new OptionsQuestionViewModel();
            return PartialView("_OptionsQuestionPartial", optionQuestion);
        }

        // POST: OptionQuestions/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateOptionQuestion(OptionsQuestionViewModel question)
        {
                var optionQuestionDTO = _mapper.Map<OptionsQuestionDTO>(question);
                await _optionsQuestionService.CreateAsync(optionQuestionDTO);

            return PartialView("_OptionsQuestionPartial", question);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateOption(OptionViewModel option)
        {
            var optionDTO = _mapper.Map<OptionDTO>(option);
            await _optionsQuestionService.CreateOptionAsync(optionDTO);

            return View();                      
        }
    
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditOption(Guid Id,OptionViewModel option)
        {
            if (Id != option.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var optionDTO = _mapper.Map<OptionDTO>(option);
                await _optionsQuestionService.EditOptionAsync(Id, optionDTO);
                return RedirectToAction(nameof(Index));
            }

            return View(option);
        }

        // POST: OptionQuestions/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditOptionQuestion(Guid Id,OptionsQuestionViewModel question)
        {
            if (Id != question.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var questionDTO = _mapper.Map<OptionsQuestionDTO>(question);
                await _optionsQuestionService.EditAsync(Id, questionDTO);
                return RedirectToAction(nameof(Index));
            }

            return View(question);
        }

        // GET: OptionQuestions/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

          }
}