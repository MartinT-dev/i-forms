﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using I_Form.Models;
using I_Form.Services.Contracts;
using I_Form.Services.DTOEntities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Form.Controllers
{
    public class DocumentQuestionController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IDocumentQuestionService _documentQuestionService;

        public DocumentQuestionController(IMapper mapper, IDocumentQuestionService documentQuestionService)
        {
            this._mapper = mapper ?? throw new ArgumentNullException(nameof(mapper)); ;
            this._documentQuestionService = documentQuestionService ?? throw new ArgumentNullException(nameof(documentQuestionService));
        }
        // GET: OptionQuestions
        public async Task<IActionResult> Index(Guid formId)
        {
            var questionsDTO = await _documentQuestionService.GetAllAsync(formId);
            var questionsVM = _mapper.Map<ICollection<DocumentQuestionViewModel>>(questionsDTO);
            return View(questionsVM);
        }

        // GET: OptionQuestions/Details/5
        public async Task<IActionResult> Details(Guid id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var question = await _documentQuestionService.GetAsync(id);

            var questionVM = _mapper.Map<DocumentQuestionViewModel>(question);

            if (questionVM == null)
            {
                return NotFound();
            }

            return View(questionVM);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var documentQuestion = new DocumentQuestionViewModel();
            return PartialView("_DocumentQuestionPartial", documentQuestion);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateDocumentQuestion(DocumentQuestionViewModel question)
        {
            var documentQuestionDTO = _mapper.Map<DocumentQuestionDTO>(question);
            await _documentQuestionService.CreateAsync(documentQuestionDTO);

            return PartialView("_DocumentQuestionPartial", question);
        }



        // POST: OptionQuestions/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditOptionQuestion(DocumentQuestionViewModel question)
        {

            if (ModelState.IsValid)
            {
                var questionDTO = _mapper.Map<DocumentQuestionDTO>(question);
                await _documentQuestionService.EditAsync(questionDTO);
                return RedirectToAction(nameof(Index));
            }

            return View(question);
        }

        // GET: OptionQuestions/Delete/5
        public ActionResult Delete(Guid id)
        {
            return View();
        }
    }
}
