using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using I_Form.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using I_Form.Data.Entities;
using I_Form.Services;
using I_Form.Services.Contracts;
using I_Form.Services.Providers;
using I_Form.Services.Providers.Contracts;
using Microsoft.AspNetCore.Identity.UI.Services;
using AutoMapper;
using I_Form.Services.Services;
using I_Form.Services.Mappers.Profiles;
using Microsoft.AspNetCore.Mvc;
using I_Form.Models;

namespace I_Form
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<FormContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<User, Role>(options => options.SignIn.RequireConfirmedAccount = false)
                  .AddEntityFrameworkStores<FormContext>()
                  .AddDefaultTokenProviders();

            //services.AddDefaultIdentity<User>(options => options.SignIn.RequireConfirmedAccount = false)
            //  .AddRoles<Role>()
            //  .AddEntityFrameworkStores<FormContext>();

            services.AddControllersWithViews();
            services.AddAutoMapper(typeof(Startup).Assembly,typeof(FormService).Assembly,typeof(FormViewModel).Assembly);

   

            services.AddMvc(options =>
            {
                options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute());
            });

            services.AddRazorPages().AddRazorRuntimeCompilation();

            services.Configure<IdentityOptions>(options =>
            {
                options.SignIn.RequireConfirmedAccount = false;
                options.Password.RequiredLength = 4;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
            });

            services.AddScoped<IUserService,UserService>();
            services.AddScoped<ITextQuestionService,TextQuestionService>();
            services.AddScoped<IOptionsQuestionService,OptionsQuestionService>();
            services.AddScoped<IDocumentQuestionService,DocumentQuestionService>();
            services.AddScoped<IFormService,FormService>();

            services.AddScoped<IDateTimeProvider,DateTimeProvider>();
            services.AddSingleton<IEmailSender, EmailSender>();

        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

  

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });
        }
    }
}
