﻿using I_Form.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace I_Form.Data.Configurations
{
    public class ResponsesConfiguration : IEntityTypeConfiguration<Responses>
    {
        public void Configure(EntityTypeBuilder<Responses> builder)
        {
            builder.HasKey(r => r.Id);
            builder.HasOne(r => r.Form)
                            .WithMany(f => f.Responses)
                            .HasForeignKey(r => r.FormId);
            
        }
    }
}
