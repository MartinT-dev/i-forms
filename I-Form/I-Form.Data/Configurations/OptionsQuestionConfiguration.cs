﻿using I_Form.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace I_Form.Data.Configurations
{
    public class OptionsQuestionConfiguration : IEntityTypeConfiguration<OptionsQuestion>
    {
 public void Configure(EntityTypeBuilder<OptionsQuestion> builder)
        {
            builder.HasKey(oq => oq.Id);

            builder.HasOne(oq => oq.Form)
                .WithMany(f => f.OptionsQuestions)
                .HasForeignKey(oq => oq.FormId);

            builder.Property(oq => oq.Title)
                .IsRequired();

     builder.Property(oq => oq.Description)
                .IsRequired();

        }

    }
}
