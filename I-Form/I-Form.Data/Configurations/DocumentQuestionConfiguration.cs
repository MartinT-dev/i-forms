﻿using I_Form.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace I_Form.Data.Configurations
{
    public class DocumentQuestionConfiguration : IEntityTypeConfiguration<DocumentQuestion>
    {
        public void Configure(EntityTypeBuilder<DocumentQuestion> builder)
        {
  builder.HasKey(dq => dq.Id);

            builder.HasOne(dq => dq.Form)
                .WithMany(f => f.DocumentQuestions)
                .HasForeignKey(dq => dq.FormId);

            builder.Property(dq => dq.Title)
                .IsRequired();

     builder.Property(dq => dq.Description)
                .IsRequired();

        }
    }
}
