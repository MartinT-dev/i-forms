﻿using I_Form.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Text;

namespace I_Form.Data.Configurations
{
    public class TextQuestionAnswersConfiguration : IEntityTypeConfiguration<TextQuestionAnswers>
    {
        public void Configure(EntityTypeBuilder<TextQuestionAnswers> builder)
        {
            builder.HasKey(tqa => new { tqa.ResponseId, tqa.QuestionId });

            builder.Property(tqa => tqa.Answer).IsRequired();
        }
    }
}
