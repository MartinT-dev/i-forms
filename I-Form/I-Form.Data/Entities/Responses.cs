﻿using I_Form.Data.Abstract;
using I_Form.Data.Abstract.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace I_Form.Data.Entities
{
    public class Responses  
    {
        public Guid Id { get; set; }

        public Guid FormId { get; set; }
        public Form Form { get; set; }
        public ICollection<DocumentQuestionAnswers> DocumentQuestionAnswers { get; set; }
        public ICollection<TextQuestionAnswers> TextQuestionAnswers{ get; set; }
        public ICollection<OptionsQuestionAnswers> OptionsQuestionAnswers{ get; set; }
        public DateTime CreatedOn { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
    }
}
