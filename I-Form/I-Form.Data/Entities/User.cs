﻿using I_Form.Data.Abstract.Contracts;
using I_Form.Data.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Text;

namespace I_Form.Data
{
    public class User : IdentityUser<Guid>,IDeletable,IAuditable
    {
        public ICollection<Form> Forms { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}


