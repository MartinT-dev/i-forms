﻿using I_Form.Data.Abstract;
using I_Form.Data.Abstract.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace I_Form.Data.Entities
{
    public class DocumentQuestion : IEntity, IAuditable, IDeletable
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public ICollection<DocumentQuestionAnswers> DocumentQuestionAnswers {get;set;}
        public Guid? FormId { get; set; }
        public Form Form { get; set; }
        public bool IsRequired { get; set; }
        public int FileNumberLimit { get; set; }
        public int FileSizeLimit { get; set; }
           }
}
