﻿using I_Form.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace I_Form.Data.Seeder
{
    public static class ModelBuilderExtension
    {
        public static void Seeder(this ModelBuilder builder)
        {
            var id = Guid.NewGuid();
            var txtQid1 = Guid.NewGuid();

            //User
            builder.Entity<User>().HasData(
                new User
                {
                    Id = id,
                    UserName = "TestUser1",
                });

            var formId = Guid.NewGuid();

            builder.Entity<Form>().HasData(
                new Form
                {
                    Id = formId,
                    Title = "Test Form",
                    Description = "Test Description",
                });
                

            builder.Entity<TextQuestion>().HasData(
                new TextQuestion
                {
                    Id = txtQid1,
                    FormId = formId,
                    Title = "Test Title",
                    Description = "Test Description"
                    
                });


            ////Role
            //builder.Entity<Role>().HasData(
            //    new Role { Id = id, Name = "Admin", NormalizedName = "ADMIN" },
            //    new Role { Id = id2, Name = "User", NormalizedName = "USER" }
            //);
          
        }
    }
}