﻿using I_Form.Services.DTOEntities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace I_Form.Services.Contracts
{
    public interface IUserService
    {
        Task<UserDTO> GetUserAsync(int id);
    }
}
