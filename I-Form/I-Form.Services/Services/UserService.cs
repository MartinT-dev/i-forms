﻿using I_Form.Data;
using I_Form.Services.Contracts;
using I_Form.Services.DTOEntities;
using I_Form.Services.Providers.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace I_Form.Services
{
    public class UserService : IUserService
    {
        private readonly FormContext context;
        private readonly IDateTimeProvider dateTimeProvider;

        public UserService(FormContext context, IDateTimeProvider dateTimeProvider)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));
        }

        public Task<UserDTO> GetUserAsync(int id)
        {
            throw new NotImplementedException();
        }
    }
}
