﻿using AutoMapper;
using I_Form.Data;
using I_Form.Data.Entities;
using I_Form.Services.Contracts;
using I_Form.Services.DTOs;
using I_Form.Services.Mappers;
using I_Form.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace I_Form.Services
{
    public class TextQuestionService : ITextQuestionService
    {

        #region fields
        private readonly FormContext _context;
        private readonly IDateTimeProvider dateTimeProvider;
        private readonly IMapper _mapper;
        #endregion

        public TextQuestionService(FormContext context, IDateTimeProvider dateTimeProvider, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            this.dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));
            this._mapper = mapper;
        }

        /// <summary>
        /// Create method that uses a DTO that it's comming from the user's input.
        /// </summary>
        /// <param name="textQuestionDTO">The DTO that is used to mapp the entity that is going to be saved in the database.</param>
        /// <returns>
        /// Text Question DTO.
        /// </returns>
        public async Task<TextQuestionDTO> CreateTextQuestionAsync(TextQuestionDTO textQuestionDTO)
        {
            #region check for existing 
            var check = await this._context.TextQuestions
                 .FirstOrDefaultAsync(tQ => tQ.Title == textQuestionDTO.Title);

            if (check != null)
            {
                throw new ArgumentException("Text question with the same Tittle already exists!");
            }
            #endregion

            var id = Guid.NewGuid();

            var textQuestion = new TextQuestion
            {
                Id = id,
            };

            textQuestionDTO.Id = id;

            textQuestion = _mapper.Map<TextQuestion>(textQuestionDTO);


            await _context.TextQuestions.AddAsync(textQuestion);
            await _context.SaveChangesAsync();


            return textQuestionDTO;
        }

        /// <summary>
        /// Delete method that uses the entity Id to delete it.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Bool</returns>
        public async Task<bool> DeleteTextQuestionAsync(Guid id)
        {
            try
            {
                var textQuestion = await this._context.TextQuestions
                    .Where(tQ => tQ.IsDeleted == false)
                    .FirstOrDefaultAsync(tQ => tQ.Id == id);

                textQuestion.IsDeleted = true;
                textQuestion.FormId = null;

                textQuestion.DeletedOn = this.dateTimeProvider.GetDateTime();
                await this._context.SaveChangesAsync();

                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        /// <summary>
        /// Get method that returns a collection of TextQuestiond DTOs.
        /// </summary>
        /// <returns>Collection of TextQuestionDTOs</returns>
        public async Task<ICollection<TextQuestionDTO>> GetAllTextQuestionAsync()
        {
            var textQuestions = await this._context.TextQuestions
                .Where(tQ => tQ.IsDeleted == false)
                .ToListAsync();
           
            var textQuestionDTOs = _mapper.Map<List<TextQuestionDTO>>(textQuestions);

            return textQuestionDTOs;
        }
        public async Task<ICollection<TextQuestionDTO>> GetAllTextQuestionAsync(Guid formId)
        {
            var textQuestions = await this._context.TextQuestions
                .Where(tQ => tQ.IsDeleted == false && tQ.Id == formId)
                .ToListAsync();
           
            var textQuestionDTOs = _mapper.Map<List<TextQuestionDTO>>(textQuestions);

            return textQuestionDTOs;
        }


        /// <summary>
        /// Get method that uses the entity's Id and returns a DTO
        /// </summary>
        /// <param name="id"></param>
        /// <returns>
        /// Text Question DTO
        /// </returns>
        public async Task<TextQuestionDTO> GetTextQuestionAsync(Guid id)
        {
            var textQuestion = await this._context.TextQuestions
                 .Where(tq => tq.IsDeleted == false)
                 .FirstOrDefaultAsync(tq => tq.Id == id);
            if (textQuestion.Equals(null))
            {
                throw new ArgumentNullException("Text question does not exist!");
            }

            var textQuestionDTO = _mapper.Map<TextQuestionDTO>(textQuestion);

            return textQuestionDTO;

        }

        /// <summary>
        /// Update method that updates the question's Title and Description
        /// </summary>
        /// <param name="id"></param>
        /// <param name="newTitle"></param>
        /// <param name="newDescription"></param>
        /// <returns>
        /// Text Question DTO
        /// </returns>
        public async Task<TextQuestionDTO> UpdateTextQuestionAsync(Guid id, string newTitle, string newDescription)
        {
            var textQuestion = await this._context.TextQuestions
                .Where(tQ => tQ.IsDeleted == false)
                .FirstOrDefaultAsync(tQ => tQ.Id == id);

            textQuestion.Title = newTitle;
            textQuestion.Description = newDescription;
            textQuestion.UpdatedOn = this.dateTimeProvider.GetDateTime();

            await this._context.SaveChangesAsync();

            var textQuestionDTO = _mapper.Map<TextQuestionDTO>(textQuestion);

            return textQuestionDTO;

        }

     
    }
}
