﻿using AutoMapper;
using I_Form.Data.Entities;
using I_Form.Services.DTOEntities;
using System;
using System.Collections.Generic;
using System.Reflection.Metadata;
using System.Text;

namespace I_Form.Services.Mappers.Profiles
{
   public class DocumentQuestionProfile : Profile
    {
        public DocumentQuestionProfile()
        {
            CreateMap<DocumentQuestion, DocumentQuestionDTO>().ReverseMap();
            CreateMap<ICollection<DocumentQuestion>, ICollection<DocumentQuestionDTO>>().ReverseMap();
        }

    }
}
