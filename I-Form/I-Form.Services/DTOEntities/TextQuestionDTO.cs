﻿using System;
using System.Collections.Generic;
using System.Text;

namespace I_Form.Services.DTOs
{
    public class TextQuestionDTO
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Guid FormId { get; set; }
        public string Form { get; set; }
        public ICollection<TextQuestionAnswersDTO> TextQuestionAnswers { get; set; }
        public bool IsRequired { get; set; }
        public bool IsLongAnswer { get; set; }
    }
}
