﻿using System;
using System.Collections.Generic;
using System.Text;

namespace I_Form.Services.DTOs
{
    public class TextQuestionAnswersDTO
    {
        public Guid QuestionId { get; set; }
        public string Question { get; set; }
        public Guid ResponseId { get; set; }
        public string Responses { get; set; }
        public string Answer { get; set; }
    }
}
