﻿using I_Form.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace I_Form.Services.DTOEntities
{
    public class OptionsQuestionDTO
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public ICollection<OptionsQuestionAnswers> Answers { get; set; }
        public ICollection<Option> Options { get; set; }
        public Guid FormId { get; set; }
        public Form Form { get; set; }
        public bool IsRequired { get; set; }
        public bool IsMultipleChoice { get; set; }

    }
}
